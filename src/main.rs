//! # Introduction
//!
//! `udpupd` is a UDP based uptime monitor being developed for p2p architecture.
//!
//! Any host running udpupd is a server, and optionally also a client.
//!
//! The behavior of the component is defined in a single YAML file. On invocation, the location of
//! this YAML file may be specified using the `-c` flag. By default it looks for file `udpupd.yaml`
//! in the current working directory. A panic will occur if that file cannot be found.
//!
//!
//! ## Server configuration
//!
//! The server **MUST** be configured in the YAML file. The configuration is defined at the root level
//! of the document, as follows:
//!
//! ``` 
//! server:
//!   host: <host_ip>
//!   port: <host_port>
//! ```
//!
//! `<host_ip>` and `<host_port>` should here be substituted by the actual ip address and port that
//! the host will serve uptime information from.
//!
//!
//! ## Client configuration
//!
//! The process may _optionally_ trace the uptime of other peers, specified in the YAML config
//! files in the `client` section.
//!
//! If defined the `client` section **MUST** contain the following:
//!
//! ```
//! client:
//!   host: <client_ip>
//! ```
//!
//! Where the `<client_ip>` is the ip address that the client will use to send uptime queries to
//! the remote peer(s).
//!
//! The `client` definition **SHOULD** contain one or more `remotes` definitions, specifying which
//! remote hosts to query uptime for.
//!
//! A full example of a host querying its own uptime can be:
//!
//! ```
//! server:
//!   host: 127.0.0.1
//!   port: 8000
//! client:
//!   host: 127.0.0.1
//!   remotes:
//!     - host: 127.0.0.1
//!       port: 8000
//! ```
//!
//!
//! ## Debugging
//!
//! `udpupd` uses [env_logger]. The environment variable `RUST_LOG` can be used to specify the log
//! level. The loglevels conform to the [log] interface.


use std::{
    fs,
    fmt,
    env,
    thread,
    time,
    path,
};

#[cfg(target_os = "linux")]
use std::os::unix::fs::MetadataExt;

use std::net::UdpSocket;

use udpupd::mock;

use std::net::SocketAddrV4;
use std::str::FromStr;
use std::sync::mpsc;

use signal_hook::consts as sig_consts;
use signal_hook::iterator::Signals;
use rand::{
    Rng,
}; 

use log::{
    debug,
    info,
    warn,
    error,
};
use env_logger;

use yaml_rust::{
    YamlLoader,
    Yaml,
};
use yaml_rust::yaml::{
    Hash,
}; 

use clap;


/// Defines a host/port pair of any agent part of the network.
struct Settings {
    host: String,
    port: u16,
}

impl Settings {
    pub fn new(host: &str, port: u16) -> Settings {
        return Settings {
            host: host.to_string(),
            port: port,
        }
    }

    /// Returns the string representation of the resource, suitable to pass to a socket
    /// constructor.
    pub fn as_bind_string(&self) -> String {
        return format!("{}:{}", self.host, self.port);
    }

    /// Same as [Settings::as_bind_string], but allowing for a randomized port.
    pub fn as_random_port_string(&self) -> String {
        return format!("{}:0", self.host);
    }
}

impl fmt::Display for Settings {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "host {} port {}", self.host, self.port)
    }
}


/// State for the running uptime server on the current peer.
struct Server {
    /// Socket listening to uptime requests from peers.
    socket: UdpSocket,
    /// When set to false, process must terminate.
    run: bool,
}


impl Server {
    pub fn new(settings: Settings) -> Server {
        let socket_url: String = settings.as_bind_string();
        return Server {
            socket: UdpSocket::bind(socket_url).unwrap(),
            run: true,
        }
    }

    /// Starts the server's processing loop.
    /// If the receiving end of the `stop_ch` channel is closed, the server loop will exit.
    pub fn start(&mut self, stop_ch: mpsc::Sender<bool>) {
        while self.run {
            let r = stop_ch.send(true);
            match r {
                Ok(()) => {
                    self.doit();
                },
                Err(mpsc::SendError(x)) => {
                    debug!("shutting down");
                    self.run = false;
                }
            }
        }
    }

    /// Executes a single receive on the socket.
    /// Echoes the provided data as identifier of the response.
    pub fn doit(&mut self) {
        let mut b: [u8; 8] = [0; 8];
        let r = self.socket
            .recv_from(&mut b);
        match r {
            Ok((c, sock_addr)) => {
                let v = &b[..c];
                debug!("ping {:?} from host {} port {}", v, sock_addr.ip(), sock_addr.port());
                self.socket.send_to(v, sock_addr);
            },
            Err(e) => {},
        };
    }
}


/// Representation of a remote peer for which to track uptime.
struct Client {
    /// [Settings] specification of peer.
    peer_settings: Settings,
    /// [Settings] specification of local connection to peer.
    our_settings: Settings,
    /// Timeout after which peer will be considered _down_ for a single query.
    read_timeout: time::Duration,
    /// The latest identifier (`nonce`) sent as part of query to the peer.
    nonce: [u8; 8],
    /// The textual representation of the local node, used for display purposes.
    identifier: String,
}


// client should optionally define ipv6 address
// ipv4 can be defined in ipv6 format:
// https://docs.oracle.com/cd/E19683-01/817-0573/transition-4/index.html
// "up" should be per-address since there is no way of knowing whether peer split them on multiple
// hosts, nor stopped supporting one or the other

impl Client {
    pub fn new(us: Settings, them: Settings) -> Client {
        let identifier = them.host.clone();
        
        Client{
            peer_settings: them,
            our_settings: us,
            read_timeout: time::Duration::from_secs(1),
            nonce: [0u8; 8],
            identifier: identifier,
        }
    }

    /// Execute a single uptime query against the [Client].
    /// 
    /// If the query is successful, the timestamp at the time when query receipt was received will
    /// be returned. Otherwise, 0 will be returned.
    pub fn ping(&mut self) -> u64 {
        let mut rng = rand::thread_rng();
        rng.fill(&mut self.nonce);
        
        let sock_local_url: String = self.our_settings.as_random_port_string();
        let sock_local = UdpSocket::bind(&sock_local_url).unwrap();
        sock_local.set_read_timeout(Some(self.read_timeout));
        
        let sock_url_remote: String = self.peer_settings.as_bind_string();
        let sock_addr_remote = SocketAddrV4::from_str(&sock_url_remote).unwrap();
        debug!("local url {} remote {}", sock_local_url, sock_addr_remote);
        debug!("vvvv {:?}", self.nonce);
        let v = sock_local.send_to(&mut self.nonce, sock_addr_remote);
        let r = v.unwrap();
        debug!("sendto {:?} id {:?} result {}", sock_addr_remote, self.identifier, r);

        let mut b = [0; 8];
        let rr = sock_local.recv(&mut b);
        match rr {
            Ok(c) => {
                debug!("pong {} bytes {:?} from {:?}", c, b, self.identifier);
                let now = time::SystemTime::now();
                let timestamp = now.duration_since(time::UNIX_EPOCH);
                timestamp.unwrap().as_secs()
            },
            Err(e) => {
                warn!("no answer from {}", self.peer_settings);
                0
            },
        }
    }
}

impl fmt::Display for Client {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.our_settings)
    }
}


/// Handle termination signal, closing the threaded [Server] and terminating [Client] queries.
fn handle_sig(tx: mpsc::Sender<bool>) {
    let mut sigs = Signals::new(&[sig_consts::SIGINT]).unwrap();
    for sig in &mut sigs {
        tx.send(true);
        return;
    }
}


/// Ingests the top-level YAML config and returns the server configuration part as
/// [yaml_rust::yaml::Hash].
fn to_server_config(v: &Hash) -> &Hash {
    let mut k = Yaml::from_str("server");
    let r = v.get(&k).unwrap().as_hash().unwrap();
    return r;
}


/// Instantiates a [Server] from the output from [to_server_config].
fn server_from_config(server_config: &Hash) -> Server {
    let mut k = Yaml::from_str("host");
    let mut host = server_config.get(&k).unwrap().as_str().unwrap();
    k = Yaml::from_str("port");
    let mut port: u16 = server_config.get(&k).unwrap().as_i64().unwrap() as u16;
    let mut server_settings = Settings::new(&host, port);
    info!("Server: {}", server_settings);
    Server::new(server_settings)
}

/// Ingests the top-level YAML config and returns the client configurations part as
/// [core::option::Option]<[yaml_rust::yaml::Hash]>.
fn to_client_config(v: &Hash) -> Option<&Hash> {
    let mut k = Yaml::from_str("client");
    let r = v.get(&k);
    match r {
        Some(vv) => {
            return Some(vv.as_hash().unwrap());
        },
        None => {
            return None;
        }
    }
}


/// Instantiates a [std::vec::Vec]<[Client]> from the output of [to_client_config] and [to_server_config].
///
/// If there are no clients, the returned vector will be empty, and only the server thread should
/// be running as a result.
fn clients_from_config(local_config: Option<&Hash>, server_config: &Hash) -> Vec<Client> {
    let mut local_settings: Option<Settings> = None;

    match local_config {
        Some(config) => {
            let local_config_hash = config;

            let mut k = Yaml::from_str("host");
            let host = config.get(&k).unwrap().as_str().unwrap();
            debug!("local config {:?}", local_config);
            let port = 0;

            let local_settings_content = Settings::new(&host, port);
            info!("Client source {}", local_settings_content);
            local_settings = Some(local_settings_content);
        },
        None => {
            return vec!()
        },
    }

    let local_settings_content = local_settings.unwrap();
    let mut clients: Vec<Client> = vec![];

    let k = Yaml::from_str("remotes");
    let remotes = local_config.unwrap().get(&k);

    match remotes {
        Some(remote) => {
            for endpoint in remote.as_vec().unwrap() {
                let endpoint_config = endpoint.as_hash().unwrap();

                let mut k = Yaml::from_str("host");
                let host: &str = endpoint_config.get(&k).unwrap().as_str().unwrap();
                k = Yaml::from_str("port");
                let port: u16 = endpoint_config.get(&k).unwrap().as_i64().unwrap() as u16;
                let them: Settings = Settings::new(&host, port);

                let us_settings = Settings{
                    host: local_settings_content.host.clone(),
                    port: local_settings_content.port.clone(),
                };
                let client: Client = Client::new(us_settings, them);
                clients.push(client);
                info!("Adding client host {} port {}", host, port);
            }
        },
        None => {},
    };

    return clients;
}


/// Execute a single query against a peer to assert up status.
///
/// On a successful query the timestamp for when the reply was received will be written to the
/// corresponding file in the given `data_dir`.
fn client_ping(data_dir: &path::Path, c: &mut Client) {
    let v = c.ping();
    if v > 0 {
        debug!("recv server @ {} reply @ {}", &c.peer_settings, v);
        let timestamp_bytes = v.to_be_bytes();
        let fp = data_dir.join(&c.identifier);
        fs::write(fp, timestamp_bytes);
    }
}

/// Get a valid rundir for the current platform
///
/// Currently only supports linux /var/run/user
#[cfg(target_os = "linux")]
fn rundir_from_config(y: Option<&Hash>) -> path::PathBuf {
    let k = "UID";
    // thx https://stackoverflow.com/questions/57951893/how-to-determine-the-effective-user-id-of-a-process-in-rust
    let uid = std::fs::metadata("/proc/self").unwrap().uid(); //map(|m| m.uid().unwrap());
    let s = format!("/run/user/{}/udpupd", uid);
    let mut p = path::Path::new(&s).to_path_buf();

    match y {
        Some(v) => {
            let k = Yaml::from_str("rundir");
            match v.get(&k) {
                Some(vv) => {
                    let path_string = vv.as_str().unwrap();
                    p = path::Path::new(&path_string).to_path_buf(); 
                },
                _ => {},
            };
        },
        _ => {},
    };
    p
}

/// By default the executable will read configuration from `./udpupd.yaml`.
/// For more details, run `cargo run -- -h`
fn main() {
    // Apply loglevel
    env_logger::init();

    // Parse cli args
    let m = clap::App::new("udpupd")
        .version("0.0.1")
        .arg(clap::Arg::with_name("config")
             .short("c")
             .long("config")
             .value_name("YAML configuration file")
             .takes_value(true)
             )
        .get_matches();

    // Load the configuration
    let config_file = m.value_of("config").unwrap_or("udpupd.yaml");
    info!("reading from config {}", config_file);
    let s = fs::read_to_string(&config_file).unwrap();
    let y = YamlLoader::load_from_str(&s).unwrap();
    let v = y[0].as_hash().unwrap();
    let server_config = to_server_config(v);


    // Set up server
    let mut z = server_from_config(server_config);
    
    // Start server in new thread.
    let (quit_tx, quit_rx) = mpsc::channel();
    thread::spawn(move || {
        z.start(quit_tx);
    });

    // Connect signal handler from server.
    let (sig_tx, sig_rx) = mpsc::channel();
    thread::spawn(move || {
        handle_sig(sig_tx);
    });

    // Process client configurations.
    let client_config = to_client_config(v);
    let mut clients = clients_from_config(client_config, server_config);

    // In case no clients are defined, we are only running the server.
    // Intercept code execution and short-circuit when quit signal is detected.
    if clients.len() == 0 {
        let r = sig_rx.recv();
        return;
    }

    // Make sure we have a data directory to store the query timestamps
    let data_dir = rundir_from_config(client_config); //path::Path::new("udpupd_dat");
    info!("using data dir {:?}", data_dir);
    let r = fs::create_dir_all(&data_dir).unwrap();

    // Deduce the client polling delay from config
    let mut delay: u64 = 2000;
    let k = Yaml::from_str("delay");
    match client_config.unwrap().get(&k) {
        Some(v) => {
            let v_delay: i64 = v.as_i64().unwrap();
            delay = v_delay as u64;
        },
        _ => {},
    };

    // If we have clients, execute loop polling the individual clients in round-robin fashion.
    loop {
        debug!("entering client loop");
        
        // If the termination signal is caught, make sure we exit the loop.
        //
        // Also make sure that we report suspicious circumstances in logging.
        let r = sig_rx.try_recv();
        match r {
            Ok(_) => {
                info!("termination");
                drop(&quit_rx);
                break;
            },
            Err(mpsc::TryRecvError::Disconnected) => {
                error!("unexpected disconnect from signal handler");
                break;    
            },
            Err(mpsc::TryRecvError::Empty) => {
            },
        }

        for c in &mut clients {
            client_ping(&data_dir, c);
        }

        let t = time::Duration::from_millis(delay);
        thread::sleep(t);
    }
}


#[cfg(test)]
mod tests {
    use super::{
        Settings,
        to_server_config,
        to_client_config,
        clients_from_config,
        rundir_from_config,
    };

    use yaml_rust::{
        YamlLoader,
    };

    #[test]
    fn test_rundir() {
        let s = "client: \n\
\x20\x20rundir: /foo/bar \n\
";
        let y = YamlLoader::load_from_str(&s).unwrap();
        let v = y[0].as_hash().unwrap();
        let c = to_client_config(&v);
        let p = rundir_from_config(c);

    }

    #[test]
    fn test_init() {
        let s: Settings = Settings::new("127.0.0.1", 6167);
        assert_eq!("127.0.0.1:6167", s.as_bind_string());
    }

    #[test]
    fn test_server_init() {
        let s = "server: \n
\x20\x20host: 127.0.0.1 \n
\x20\x20port: 4444 \n
";
        let y = YamlLoader::load_from_str(&s).unwrap();
        let v = y[0].as_hash().unwrap();
        let c = to_server_config(&v);
    }

    #[test]
    fn test_client_init() {
        let s = "client: \n
\x20\x20host: 127.0.0.1 \n
";
        let y = YamlLoader::load_from_str(&s).unwrap();
        let v = y[0].as_hash().unwrap();
        let c = to_client_config(&v);
    }

    #[test]
    fn test_client_init_no_remotes() {
        let s = "server:\n
\x20\x20host: 127.0.0.1 \n
\x20\x20port: 4444 \n
client: \n
\x20\x20host: 127.0.0.1 \n
";
        let y = YamlLoader::load_from_str(&s).unwrap();
        let v = y[0].as_hash().unwrap();
        let server_config = to_server_config(&v);
        let client_config = to_client_config(&v);
        let clients = clients_from_config(client_config, server_config);
        assert_eq!(clients.len(), 0);

    }

    #[test]
    fn test_client_init_with_remotes() {
        let s = "server:\n
\x20\x20host: 127.0.0.1 \n
\x20\x20port: 4444 \n
client: \n
\x20\x20host: 127.0.0.1 \n
\x20\x20remotes: \n
\x20\x20\x20\x20- host: 127.0.1.1\n
\x20\x20\x20\x20\x20\x20port: 5555\n
";
        let y = YamlLoader::load_from_str(&s).unwrap();
        let v = y[0].as_hash().unwrap();
        let server_config = to_server_config(&v);
        let client_config = to_client_config(&v);
        let clients = clients_from_config(client_config, server_config);
        assert_eq!(clients.len(), 1);
    }
}
