struct UdpSocket {
    data: [u8; 128],
    l: usize,
}

impl UdpSocket {
    pub fn bind(s: String) -> UdpSocket {
        UdpSocket{
            data: [0u8; 128],
            l: 0,
        }
    }
}
